README

# ZUP Teste #

App para Android criado como teste enviado pela ZUP Innovations para a emiolo.com.

### Funcionalidades ###

O App contém as seguintes funcionalidades:

* Cadastro de filme;
* Listagem dos filmes salvos;
* Acesso à API do IMDb (omdb);

### Cadastro de filme acessando a API do IMDb (omdb) ###

![Search_1.png](https://bitbucket.org/repo/oXj9ep/images/2713837797-Search_1.png) ![Search_2.png](https://bitbucket.org/repo/oXj9ep/images/1935741179-Search_2.png)

### Listagem dos filmes salvos ###

![Home.png](https://bitbucket.org/repo/oXj9ep/images/2559547890-Home.png)

# Desenvolvido por #

### emiolo.com ###