package com.movie.imdb;

import android.app.Application;
import android.content.Context;

import com.orm.SugarContext;

public class IMDBApplication extends Application {

    private static IMDBApplication instance;

    public IMDBApplication() {
        instance = this;
    }

    @Override
    public void onCreate() {

        super.onCreate();
        SugarContext.init(this);
    }

    @Override
    public void onTerminate() {

        super.onTerminate();
        SugarContext.terminate();
    }

    public static IMDBApplication getInstance() {
        return instance;
    }

    public static Context getContext() {

        return IMDBApplication.getInstance().getApplicationContext();
    }
}
