package com.movie.imdb.controller;

import com.movie.imdb.Filters.FilterRequestPending;
import com.movie.imdb.Filters.FilterRequestPendingListener;
import com.movie.imdb.IMDBApplication;
import com.movie.imdb.interfaces.AbstractRequestListener;
import com.movie.imdb.interfaces.SpringAndroidSpiceRequestInterface;
import com.movie.imdb.model.IMDBSpringSpiceService;
import com.movie.imdb.model.RequestPending;
import com.movie.imdb.utils.JCollections;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class RequestController {

    private static SpiceManager spiceManager;
    private List<RequestPending> pendingRequestList;

    public RequestController() {

        pendingRequestList = new ArrayList<RequestPending>(0);

        if(spiceManager == null) {

            spiceManager = new SpiceManager(IMDBSpringSpiceService.class);
        }
    }

    public final void start() {

        if(!spiceManager.isStarted()) {

            spiceManager.start(IMDBApplication.getInstance());
        }
    }

    public final void shouldStop() {

        if(spiceManager.isStarted()) {

            spiceManager.shouldStop();
        }
    }

    public final List<RequestPending> getPendingRequestList() {

        return pendingRequestList;
    }

    public boolean hasRequestSend() {

        return (pendingRequestList != null && !pendingRequestList.isEmpty());
    }

    public boolean hasRestorableRequestForListener(AbstractRequestListener abstractRequestListener) {

        if (abstractRequestListener == null || pendingRequestList == null || pendingRequestList.isEmpty()) {

            return false;
        }

        return JCollections.findFirstItemFilter(pendingRequestList, new FilterRequestPendingListener(abstractRequestListener)) != null;
    }

    public final <T> void execute(final SpringAndroidSpiceRequestInterface request, final RequestListener<T> requestListener) {

        if(request == null || requestListener == null) {

            return;
        }

        final RequestPending requestPending = new RequestPending(((SpringAndroidSpiceRequest) request).getResultType(),
                request.createCacheKey(), request.getTimeCache(), "" + requestListener.getClass());

        RequestListener<T> mRequestListener = new RequestListener<T>() {

            @Override
            public void onRequestFailure(final SpiceException spiceException) {

                updatePendingRequestList(false, requestPending);
                requestListener.onRequestFailure(spiceException);
            }

            @Override
            public void onRequestSuccess(final T data) {

                updatePendingRequestList(false, requestPending);
                requestListener.onRequestSuccess(data);
            }
        };

        spiceManager.execute((SpringAndroidSpiceRequest) request, requestPending.getCacheKey(),
                requestPending.getCacheTimeMillis(), mRequestListener);

        updatePendingRequestList(true, requestPending);
    }

    public final void clearPendingRequestList() {

        updatePendingRequestList(true, null);
    }

    public final void setPendingRequestList(List<RequestPending> pendingRequestList) {

        if (pendingRequestList != null) {

            updatePendingRequestList(true, pendingRequestList.toArray(new RequestPending[pendingRequestList.size()]));
        }
    }

    public final void addAllListenerIfPending(AbstractRequestListener... requestListeners) {

        if (requestListeners == null || requestListeners.length <= 0) {

            clearPendingRequestList();
            return;
        }

        removeAllItensWithoutListeners(requestListeners);

        for (AbstractRequestListener requestListener : requestListeners) {

            RequestPending requestPending = JCollections.findFirstItemFilter(pendingRequestList,
                    new FilterRequestPendingListener(requestListener));

            if (requestPending != null) {

                restoreRequest(requestPending, requestListener);
            }
        }
    }

    private final <T> void restoreRequest(final RequestPending requestPending, final AbstractRequestListener<T> requestListener) {

        if (requestListener == null && requestPending == null) {

            return;
        }

        final PendingRequestListener<T> pendingRequestListener = new PendingRequestListener<T>() {

            boolean cacheLoad = false;

            @Override
            public void onRequestNotFound() {

                if (!cacheLoad) {

                    cacheLoad = true;

                    spiceManager.getFromCache(requestPending.getResultTypeClass(),
                            requestPending.getCacheKey(), DurationInMillis.ALWAYS_RETURNED, this);
                    return;
                }

                updatePendingRequestList(false, requestPending);
                requestListener.onRequestNotFound();
            }

            @Override
            public void onRequestFailure(final SpiceException spiceException) {

                updatePendingRequestList(false, requestPending);
                requestListener.onRequestFailure(spiceException);
            }

            @Override
            public void onRequestSuccess(final T data) {

                updatePendingRequestList(false, requestPending);
                requestListener.onRequestSuccess(data);
            }
        };

        spiceManager.addListenerIfPending(requestPending.getResultTypeClass(), requestPending.getCacheKey(), pendingRequestListener);
    }

    private final void updatePendingRequestList(boolean add, RequestPending... requestsPending) {

        updatePendingRequestList(false, add, requestsPending);
    }

    private final synchronized void updatePendingRequestList(Boolean clear, Boolean add, RequestPending... requestsPending) {

        if (clear != null && clear) {

            if (pendingRequestList != null) {

                pendingRequestList.clear();
            }

            return;
        }

        if (requestsPending == null || requestsPending.length <= 0) {

            return;
        }

        if (add != null && add) {

            pendingRequestList.addAll(new ArrayList<RequestPending>(Arrays.asList(requestsPending)));
            return;
        }

        JCollections.removeAllItensFilter(pendingRequestList, new FilterRequestPending(requestsPending));
    }

    private void removeAllItensWithoutListeners(AbstractRequestListener[] requestListeners) {

        JCollections.removeAllItensNotFilter(pendingRequestList,
                new FilterRequestPendingListener(requestListeners));

    }
}
