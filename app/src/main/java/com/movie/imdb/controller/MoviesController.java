package com.movie.imdb.controller;

import android.os.Handler;
import android.os.Looper;

import com.movie.imdb.interfaces.AbstractRequestListener;
import com.movie.imdb.interfaces.SugarListener;
import com.movie.imdb.model.Movie;
import com.movie.imdb.model.Plot;
import com.movie.imdb.model.Response;
import com.movie.imdb.model.request.FindMovieRequest;

import java.util.List;

public class MoviesController {

    private RequestController requestController;
    private Handler handler;

    public MoviesController(RequestController requestController) {

        handler = new Handler(Looper.getMainLooper());
        this.requestController = requestController;
    }

    public void searchMovie(final String titleMovie, final AbstractRequestListener<Movie> listener) {

        FindMovieRequest request = new FindMovieRequest();
        request.setTitle(titleMovie);
        request.setPlot(Plot.FULL);
        request.setResponse(Response.JSON);
        request.setTomatoes(true);

        requestController.execute(request, listener);
    }

    public void saveMovie(final Movie movie, final SugarListener sugarListener) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    List<Movie> movies = Movie.find(Movie.class, "title = ?", movie.getTitle());

                    if(movies.isEmpty()) {

                        movie.save();
                    }

                    postSucess(sugarListener, movie);

                } catch (final Exception exception) {

                    postError(exception, sugarListener);
                }
            }

        }).start();
    }

    public void listAllMovies(final SugarListener sugarListener) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    List<Movie> movieList = Movie.listAll(Movie.class);
                    postSucess(sugarListener, movieList.toArray(new Movie[movieList.size()]));

                } catch (final Exception exception) {

                    postError(exception, sugarListener);
                }
            }
        }).start();

    }

    //==============================================================================================
    // Métodos privados
    //==============================================================================================

    private synchronized void postSucess(final SugarListener sugarListener, final Movie... movies) {

        if (sugarListener == null) {

            return;
        }

        handler.post(new Runnable() {
            @Override
            public void run() {

                sugarListener.onSucess(movies);
            }
        });
    }

    private synchronized void postError(final Exception exception, final SugarListener sugarListener) {

        if (sugarListener == null) {

            return;
        }

        handler.post(new Runnable() {
            @Override
            public void run() {

                sugarListener.onError(exception);
            }
        });
    }
}
