package com.movie.imdb.utils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Field;

public class JSONUtils {


    public static final Gson getCustomGson() {

        GsonBuilder builder = new GsonBuilder();
        builder.setFieldNamingStrategy(FieldNamingPolicy.UPPER_CAMEL_CASE);

        return builder.create();
    }
}
