package com.movie.imdb.Filters;


import com.movie.imdb.model.Movie;

import org.apache.commons.lang3.StringUtils;

public class FilterMovie implements Filter<Movie> {

    private String title;

    public FilterMovie(String title) {
        this.title = title;
    }

    @Override
    public boolean match(Movie candidate) {

        return StringUtils.equals(title, candidate.getTitle());
    }
}
