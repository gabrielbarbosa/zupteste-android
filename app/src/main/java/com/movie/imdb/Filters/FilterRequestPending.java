package com.movie.imdb.Filters;


import com.movie.imdb.model.RequestPending;

public class FilterRequestPending implements Filter<RequestPending> {

    private RequestPending[] requestPendings;

    public FilterRequestPending(RequestPending... requestPendings) {

        this.requestPendings = requestPendings;
    }

    @Override
    public boolean match(RequestPending candidate) {

        if (requestPendings == null || requestPendings.length <= 0) {

            return false;
        }

        for (RequestPending requestPending : requestPendings) {

            if (requestPending.isEqualsCacheKey(candidate.getCacheKey())) {

                return true;
            }
        }

        return false;
    }
}
