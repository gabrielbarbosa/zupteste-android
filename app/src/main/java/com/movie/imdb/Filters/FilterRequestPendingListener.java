package com.movie.imdb.Filters;

import com.movie.imdb.model.RequestPending;
import com.octo.android.robospice.request.listener.RequestListener;

public class FilterRequestPendingListener implements Filter<RequestPending> {

    private RequestListener[] requestListeners;

    /**
     * Construtor da classe
     *
     * @param requestListeners array de listener
     */
    public FilterRequestPendingListener(RequestListener... requestListeners) {

        this.requestListeners = requestListeners;
    }

    @Override
    public boolean match(RequestPending candidate) {

        if(requestListeners == null || requestListeners.length <= 0) {

            return false;
        }

        for(RequestListener requestListener : requestListeners) {

            if(candidate.isEqualsListener(requestListener.getClass())){

                return true;
            }
        }

        return false;
    }
}
