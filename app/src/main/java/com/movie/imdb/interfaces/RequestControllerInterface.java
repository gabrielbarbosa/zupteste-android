package com.movie.imdb.interfaces;

import com.octo.android.robospice.request.listener.RequestListener;

public interface RequestControllerInterface {

    RequestListener getResRequestListener(int idListener);
}
