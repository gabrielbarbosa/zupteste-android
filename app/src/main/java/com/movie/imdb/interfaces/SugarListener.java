package com.movie.imdb.interfaces;

import com.movie.imdb.model.Movie;

public interface SugarListener {

    void onSucess(Movie... movies);

    void onError(Exception exception);
}
