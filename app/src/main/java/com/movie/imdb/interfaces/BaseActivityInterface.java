package com.movie.imdb.interfaces;

import com.movie.imdb.controller.RequestController;
import com.movie.imdb.view.UiMessages;

public abstract interface BaseActivityInterface {

	void setScreenComponents();

	void setListeners();

    boolean isPaused();

    boolean isDestroyed();

    AbstractRequestListener[] getAllRestorableRequestListeners();

    RequestController getRequestController();

    UiMessages getUiMessages();

    boolean hasRestorableRequest();

    boolean hasRestorableRequestForListener(AbstractRequestListener abstractRequestListener);
}
