package com.movie.imdb.interfaces;

import android.util.Log;

import com.movie.imdb.model.WsError;
import com.google.gson.Gson;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;

import org.json.JSONObject;

public abstract class AbstractRequestListener<T> implements RequestListener<T>, PendingRequestListener<T> {

    private static final String TAG = AbstractRequestListener.class.getSimpleName();

    @Override
    public final void onRequestFailure(final SpiceException spiceException) {

        onRequestFailure(parseToModelError(spiceException));
    }

    public abstract void onRequestFailure(WsError wsError);

    //==============================================================================================
    // Métodos privados da classe
    //==============================================================================================

    private WsError parseToModelError(SpiceException spiceException) {

        WsError wsError = null;

        try {

            wsError = new Gson().fromJson(spiceException.getCause().getMessage(), WsError.class);

        } catch (Exception ex) {

            Log.d(TAG, "" + ex.getStackTrace());
        }

        if (wsError == null) {

            // Cria com os dado padrão
            wsError = new WsError(parseToJson(spiceException));
        }

        return wsError;
    }

    private JSONObject parseToJson(SpiceException spiceException) {

        try {

            return new JSONObject(spiceException.getCause().getMessage());

        } catch (Exception e){

            return new JSONObject();
        }
    }
}
