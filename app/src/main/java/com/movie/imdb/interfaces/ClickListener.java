package com.movie.imdb.interfaces;

import android.view.View;

public interface ClickListener<T> {

    public void onItemClick(View view, int position, T data);
    public void onLongClick(View view, int position, T data);
}