package com.movie.imdb.interfaces;

public interface SpringAndroidSpiceRequestInterface {

    public String createCacheKey();

    public long getTimeCache();
}
