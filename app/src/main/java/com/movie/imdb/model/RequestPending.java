package com.movie.imdb.model;

import com.octo.android.robospice.request.listener.RequestListener;

import org.parceler.Parcel;

@Parcel
public class RequestPending {

    Class resultTypeClass;
    String cacheKey;
    String sRequestListenerClass;
    long timeMillisCache;

    public RequestPending() {
    }

    public RequestPending(Class resultTypeClass, String cacheKey, long timeMillisCache, String sRequestListenerClass) {

        this.resultTypeClass = resultTypeClass;
        this.cacheKey = cacheKey;
        this.timeMillisCache = timeMillisCache;
        this.sRequestListenerClass = sRequestListenerClass;
    }

    public Class getResultTypeClass() {
        return resultTypeClass;
    }

    public void setResultTypeClass(Class resultTypeClass) {
        this.resultTypeClass = resultTypeClass;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public String getsRequestListenerClass() {
        return sRequestListenerClass;
    }

    public void setsRequestListenerClass(String sRequestListenerClass) {
        this.sRequestListenerClass = sRequestListenerClass;
    }

    public long getCacheTimeMillis() {
        return timeMillisCache;
    }

    public void setTimeMillisCache(long timeMillisCache) {
        this.timeMillisCache = timeMillisCache;
    }

    public boolean isEqualsCacheKey(String cacheKey) {

        if (cacheKey != null && this.cacheKey != null) {

            return cacheKey.equals(this.cacheKey);
        }

        return false;
    }

    public boolean isEqualsListener(Class<? extends RequestListener> aClass) {

        if (aClass != null && sRequestListenerClass != null) {

            return sRequestListenerClass.equals("" + aClass);
        }

        return false;
    }
}