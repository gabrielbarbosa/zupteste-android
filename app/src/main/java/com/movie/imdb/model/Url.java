package com.movie.imdb.model;

public enum Url{

    BASE_URL("http://www.omdbapi.com/");

    private String description;

    private Url(String description) {
        this.description = description;
    }

    public String getDescription() {

        return description;
    }
}
