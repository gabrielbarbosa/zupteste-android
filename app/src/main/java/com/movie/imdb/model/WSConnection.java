package com.movie.imdb.model;

public final class WSConnection {

    public static final String WS_RESPONSE_MESSAGES = "messages";
    public static final String WS_RESPONSE_LOCALIZED_MESSAGES = "localized_messages";
    public static final String WS_TITLE = "t";
    public static final String WS_YEAR = "y";
    public static final String WS_PLOT = "plot";
    public static final String WS_RESPONSE = "r";
    public static final String WS_TOMATOES = "tomatoes";
}
