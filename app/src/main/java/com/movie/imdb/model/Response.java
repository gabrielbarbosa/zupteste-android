package com.movie.imdb.model;

public enum Response {

    JSON("json"), XML("xml");

    private String response;

    private Response(String plot) {

        this.response = plot;
    }

    public String getResponse(){

        return response;
    }
}
