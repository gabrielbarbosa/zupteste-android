package com.movie.imdb.model;

public enum Plot {

    SHORT("short"), FULL("full");

    private String plot;

    private Plot(String plot) {

        this.plot = plot;
    }

    public String getPlot(){

        return plot;
    }
}
