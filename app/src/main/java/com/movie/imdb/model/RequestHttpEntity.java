package com.movie.imdb.model;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;

import java.util.Collections;

public class RequestHttpEntity {

    private RequestHttpEntity() {
    }

    public static final HttpEntity getHttpEntity() {

        HttpHeaders requestHeaders = new HttpHeaders();

        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        return new HttpEntity(requestHeaders);
    }
}
