package com.movie.imdb.model;

import android.content.Context;

import com.movie.imdb.IMDBApplication;
import com.movie.imdb.R;
import com.google.gson.Gson;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.json.JSONObject;
import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Parcel
public class WsError {

    private transient Context context;
    boolean error;
    List<String> messages;
    List<String> localizedMessages;

    public WsError() {

        context = IMDBApplication.getInstance();
        error = false;
        localizedMessages = new ArrayList<String>();
        messages = new ArrayList<String>();
    }

    public WsError(JSONObject object) {

        this();

        try {

            loadMessagesWS(object);
            error = true;

        } catch (Exception e) {
        }
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<String> getMessages() {

        if ((messages == null || messages.isEmpty()) && error) {

            return Arrays.asList(context.getString(R.string.err_unspected));
        }

        return messages;
    }

    public List<String> getLocalizedMessages() {

        if ((localizedMessages == null || localizedMessages.isEmpty()) && error) {

            return Arrays.asList(context.getString(R.string.err_unspected));
        }

        return localizedMessages;
    }

    public void addLocalizedMessage(String localizedMessage) {

        if (localizedMessages != null && localizedMessage != null) {

            localizedMessages.add(localizedMessage);
        }
    }

    public void addMessage(String message) {

        if (messages != null && message != null) {

            messages.add(message);
        }
    }

    public static WsError parseToModelError(SpiceException spiceException) {

        WsError wsError = null;

        try {

            wsError = new Gson().fromJson(spiceException.getCause().getMessage(), WsError.class);

        } catch (Exception ex) {
        }

        if (wsError == null) {

            // Cria com os dados padrão
            wsError = new WsError();
            wsError.addMessage(spiceException.getLocalizedMessage());
        }

        return wsError;
    }


    // =============================================================================================
    // Métodos Privados
    // =============================================================================================

    private void loadMessagesWS(JSONObject data) {

        loadLocalizedMessages(data);
        loadMessages(data);
    }

    private void loadLocalizedMessages(JSONObject data) {

        try {

            localizedMessages = new ArrayList<String>(0);
            JSONObject wslocalizedMessages = data.getJSONObject(WSConnection.WS_RESPONSE_LOCALIZED_MESSAGES);

            for (int i = 0; i < wslocalizedMessages.length(); i++) {

                try {

                    String localizedMsg = (String) wslocalizedMessages.get(String.valueOf(i));
                    localizedMessages.add(localizedMsg);

                } catch (Exception e) {

                    e.printStackTrace();
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private void loadMessages(JSONObject data) {

        try {

            messages = new ArrayList<String>(0);
            JSONObject  wsMessages = data.getJSONObject(WSConnection.WS_RESPONSE_MESSAGES);

            for (int i = 0; i < wsMessages.length(); i++) {

                try {

                    String msg = (String) wsMessages.get(String.valueOf(i));
                    messages.add(msg);

                } catch (Exception e) {

                    e.printStackTrace();
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
