package com.movie.imdb.model;

import com.movie.imdb.IMDBApplication;
import com.movie.imdb.R;
import com.movie.imdb.interfaces.SpringAndroidSpiceRequestInterface;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.List;

public abstract class IMDBSpiceRequest<T> extends SpringAndroidSpiceRequest implements SpringAndroidSpiceRequestInterface {

    private long timeMillis;
    private IMDBParams mParams;

    /**
     * Construtor da classe, iniciliza os membros de dados
     *
     * @param clazz tipo de retorno
     */
    public IMDBSpiceRequest(Class clazz) {

        super(clazz);

        this.timeMillis = DurationInMillis.ONE_MINUTE;
        mParams = new IMDBParams();
    }

    /**
     * Construtor da classe
     *
     * @param clazz      tipo de retorno
     * @param timeMillis duração da requisição em cache
     */
    public IMDBSpiceRequest(Class clazz, long timeMillis) {

        this(clazz);
        this.timeMillis = timeMillis;
    }

    @Override
    public final long getTimeCache() {

        return timeMillis;
    }

    public void setCacheTimeMillis(long timeMillis) {

        this.timeMillis = timeMillis;
    }

    public IMDBParams getParams() {

        return mParams;
    }

    @Override
    public T loadDataFromNetwork() throws Exception {

        if (!hasAllParamsRequired()) {

            setRetryPolicy(new DefaultRetryPolicy(0, 0, 0));
            throw new SpiceException("{\"error\": true, \"data\": {}, \"localized_messages\": [\"" + IMDBApplication.
                    getContext().getString(R.string.ws_missing_parameters) + "\"]}");
        }

        ResponseEntity<T> responseEntity = null;

        try {

            responseEntity = getRestTemplate().exchange(IMDBParams.encodeParams(mParams, getUrl()), HttpMethod.GET,
                    RequestHttpEntity.getHttpEntity(), getResultType());

            return responseEntity.getBody();

        } catch (Exception exception) {

            WsError wsError = WsError.parseToModelError(new SpiceException(getExceptionMessage(exception)));

            if (wsError.getLocalizedMessages() != null && !wsError.getMessages().isEmpty() && !shouldRetryRequestIfError(wsError)) {

                setRetryPolicy(new DefaultRetryPolicy(0, 0, 0));
            }

            throw new SpiceException(getExceptionMessage(exception));
        }
    }

    @Override
    public String createCacheKey() {

        return getUrl().getDescription() + mParams.toJson();
    }

    //==============================================================================================
    // Métodos abastratos da classe
    //==============================================================================================

    protected abstract Url getUrl();

    protected abstract List<String> getAllParamsRequired();

    protected boolean shouldRetryRequestIfError(WsError wsError) {

        return false;
    }

    //==============================================================================================
    // Métodos privados da classe
    //==============================================================================================

    private boolean hasAllParamsRequired() {

        List<String> paramsRequired = getAllParamsRequired();

        if (paramsRequired == null || paramsRequired.isEmpty()) {

            return true;
        }

        for (String key : paramsRequired) {

            if (!getParams().conTains(key)) {

                return false;
            }
        }

        return true;
    }

    private String getExceptionMessage(Exception exception) {

        try {

            return ((HttpStatusCodeException) exception).getResponseBodyAsString();

        } catch (Exception e) {

            return "";
        }
    }
}
