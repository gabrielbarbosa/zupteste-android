package com.movie.imdb.model.request;

import com.google.gson.JsonObject;
import com.movie.imdb.model.IMDBSpiceRequest;
import com.movie.imdb.model.Movie;
import com.movie.imdb.model.Plot;
import com.movie.imdb.model.Response;
import com.movie.imdb.model.Url;
import com.movie.imdb.model.WSConnection;
import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.ArrayList;
import java.util.List;

public class FindMovieRequest extends IMDBSpiceRequest<String> {

    public FindMovieRequest() {

        super(Movie.class, DurationInMillis.ONE_DAY);
    }

    public void setTitle(String title) {

        getParams().addOrReplace(WSConnection.WS_TITLE, title);
    }

    public void setYear(int year) {

        getParams().addOrReplace(WSConnection.WS_YEAR, "" + year);
    }

    public void setPlot(Plot plot) {

        if (plot != null) {

            getParams().addOrReplace(WSConnection.WS_PLOT, plot.getPlot());
        }
    }

    public void setResponse(Response response) {

        if (response != null) {

            getParams().addOrReplace(WSConnection.WS_RESPONSE, response.getResponse());
        }
    }

    public void setTomatoes(boolean tomatoes) {

        getParams().addOrReplace(WSConnection.WS_TOMATOES, "" + tomatoes);
    }

    @Override
    protected Url getUrl() {

        return Url.BASE_URL;
    }

    @Override
    protected List<String> getAllParamsRequired() {

        List<String> paramsRequired = new ArrayList<String>(0);

        paramsRequired.add(WSConnection.WS_TITLE);
        paramsRequired.add(WSConnection.WS_PLOT);
        paramsRequired.add(WSConnection.WS_RESPONSE);

        return paramsRequired;
    }
}