package com.movie.imdb.model;

import android.net.Uri;

import com.google.gson.JsonObject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class IMDBParams {

    private MultiValueMap<String, String> params;

    public IMDBParams() {

        params = new LinkedMultiValueMap<String, String>();
    }

    public void add(String key, String value) {

        if (!StringUtils.isEmpty(value)) {

            params.add(key, value);
        }
    }

    public void addOrReplace(String key, String value) {

        if (StringUtils.isEmpty(value)) {

            return;
        }

        List<String> first = params.get(key);

        if (first != null) {

            first.clear();
            first.add(value);

        } else {

            add(key, value);
        }
    }

    public void remove(String key) {

        params.remove(key);
    }

    public boolean conTains(String key) {

        return params.containsKey(key);
    }

    public MultiValueMap<String, String> getMultiValueMap() {

        return params;
    }

    public static final URI encodeParams(IMDBParams params, Url url) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url.getDescription());

        Iterator<Map.Entry<String, List<String>>> iterator = params.getMultiValueMap().entrySet().iterator();

        while (iterator.hasNext()) {

            Map.Entry<String, List<String>> next = iterator.next();
            builder.queryParam(next.getKey(), next.getValue());
        }

        return builder.build().toUri();
    }

    public void clear() {

        params.clear();
    }

    public String toJson() {

        try {

            JsonObject object = new JsonObject();
            Iterator<Map.Entry<String, List<String>>> iterator = params.entrySet().iterator();

            while (iterator.hasNext()) {

                Map.Entry<String, List<String>> next = iterator.next();
                object.addProperty(next.getKey(), next.getValue().get(0));
            }

            return object.toString();

        } catch (Exception e) {

            return "";
        }
    }
}
