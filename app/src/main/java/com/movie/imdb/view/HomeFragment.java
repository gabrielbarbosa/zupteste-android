package com.movie.imdb.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.movie.imdb.R;
import com.movie.imdb.controller.MoviesController;
import com.movie.imdb.interfaces.ClickListener;
import com.movie.imdb.interfaces.SugarListener;
import com.movie.imdb.model.Movie;
import com.movie.imdb.view.adapter.MovieAdapter;
import com.movie.imdb.view.helper.NavigationHelper;

import org.parceler.Parcels;

import java.util.List;

public class HomeFragment extends BaseFragment {

    private static final String KEY_MOVIE = "key_movie";

    private MovieAdapter movieAdapter;
    private MoviesController movieController;

    // Views
    private View view;
    private RelativeLayout rlContainerLoad;
    private ProgressBar pbLoading;
    private TextView tvError;
    private RecyclerView rvMovies;
    private LinearLayoutManager linearLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.home_fragment, container, false);

        movieController = new MoviesController(getRequestController());
        movieAdapter = new MovieAdapter(getActivity());
        setScreenComponents();

        if (savedInstanceState == null) {

            loadMovies();
        }

        return view;
    }

    @Override
    public void setScreenComponents() {

        rlContainerLoad = (RelativeLayout) view.findViewById(R.id.rl_container_load);
        pbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);
        tvError = (TextView) view.findViewById(R.id.tv_error);
        rvMovies = (RecyclerView) view.findViewById(R.id.rv_movies);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvMovies.setLayoutManager(linearLayoutManager);
        rvMovies.setItemAnimator(new DefaultItemAnimator());

        rvMovies.setAdapter(movieAdapter);
    }

    @Override
    public void setListeners() {

        movieAdapter.setClickListener(itemMovieClickListener);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.fab_add_movie:

                NavigationHelper.openSearchMovies(getActivity());
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onEventMainThread(Movie movie) {

        if(movieAdapter != null && movie != null) {

            if(!movieAdapter.hasMovie(movie)) {

                movieAdapter.addItensAndNotifyDataSet(movie);
                showLoading(false);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {

        if (bundle != null) {

            bundle.putParcelable(KEY_MOVIE, Parcels.wrap(movieAdapter.getItensList()));
        }

        super.onSaveInstanceState(bundle);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle bundle) {

        super.onViewStateRestored(bundle);

        if (bundle == null) {

            return;
        }

        List<Movie> movieList = Parcels.unwrap(bundle.getParcelable(KEY_MOVIE));

        if (movieList != null) {

            movieAdapter.addItensAndNotifyDataSet(movieList.toArray(new Movie[movieList.size()]));
            showLoading(false);
        }
    }

    //==============================================================================================
    // Métodos privados
    //==============================================================================================

    private SugarListener listAllMoviesListener = new SugarListener() {
        @Override
        public void onSucess(Movie... movies) {

            if (!isAdded()) {

                return;
            }

            movieAdapter.addItens(movies);

            if(movieAdapter.isEmpty()) {

                showEmptyList();

            } else {

                showLoading(false);
            }
        }

        @Override
        public void onError(Exception exception) {

            if (!isAdded()) {

                return;
            }

            showEmptyList();
        }
    };

    private ClickListener<Movie> itemMovieClickListener = new ClickListener<Movie>() {
        @Override
        public void onItemClick(View view, int position, Movie data) {
            // TODO
        }

        @Override
        public void onLongClick(View view, int position, Movie data) {
        }
    };

    private void showLoading(boolean show) {

        if (show) {

            rlContainerLoad.setVisibility(View.VISIBLE);
            pbLoading.setVisibility(View.VISIBLE);
            tvError.setVisibility(View.GONE);
            rvMovies.setVisibility(View.GONE);

        } else {

            rlContainerLoad.setVisibility(View.GONE);
            rvMovies.setVisibility(View.VISIBLE);
        }
    }

    private void setMessageError(String message, View.OnClickListener onClickListener) {

        tvError.setText(message);
        tvError.setOnClickListener(onClickListener);

        tvError.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.GONE);
        rvMovies.setVisibility(View.GONE);
        rlContainerLoad.setVisibility(View.VISIBLE);
    }

    private void showEmptyList() {

        tvError.setText(getString(R.string.lbl_no_movie_found));
        tvError.setVisibility(View.VISIBLE);
        rlContainerLoad.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.GONE);
        rvMovies.setVisibility(View.GONE);
    }

    private void loadMovies() {

        showLoading(true);
        movieController.listAllMovies(listAllMoviesListener);
    }
}
