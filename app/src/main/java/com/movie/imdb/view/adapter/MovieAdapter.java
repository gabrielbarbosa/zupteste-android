package com.movie.imdb.view.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.movie.imdb.Filters.FilterMovie;
import com.movie.imdb.IMDBApplication;
import com.movie.imdb.R;
import com.movie.imdb.model.Movie;
import com.movie.imdb.utils.JCollections;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MovieAdapter extends RecyclerViewAdapter<Movie> {

    public MovieAdapter(Context context) {

        super(context);
        setupImageLoadConfigurations();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = getInflater().inflate(R.layout.item_movie, parent, false);

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Movie movie = getItem(position);
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        ImageLoader.getInstance().displayImage(movie.getPoster(), itemViewHolder.ivPoster);

        itemViewHolder.tvTitle.setText(movie.getTitle());
        itemViewHolder.tvDirector.setText(movie.getDirector());
        itemViewHolder.tvReleased.setText(movie.getReleased());
        itemViewHolder.tvRuntime.setText(movie.getRuntime());
        itemViewHolder.tvGenre.setText(movie.getGenre());
        itemViewHolder.tvCountry.setText(movie.getCountry());
    }

    @Override
    public int getItemCount() {

        return getItensListCount();
    }

    public boolean hasMovie(Movie movie) {

        return JCollections.findFirstItemFilter(getItensList(), new FilterMovie(movie.getTitle())) != null;
    }

    //==============================================================================================
    // Metodos privados
    //==============================================================================================

    private class ItemViewHolder extends RecyclerViewAdapter.ViewHolder implements View.OnClickListener {

        TextView tvTitle;
        TextView tvDirector;
        TextView tvReleased;
        TextView tvRuntime;
        TextView tvGenre;
        TextView tvLanguage;
        TextView tvCountry;
        ImageView ivPoster;

        public ItemViewHolder(View view) {

            super(view);

            ivPoster = (ImageView) view.findViewById(R.id.iv_poster);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvDirector = (TextView) view.findViewById(R.id.tv_director);
            tvReleased = (TextView) view.findViewById(R.id.tv_released);
            tvRuntime = (TextView) view.findViewById(R.id.tv_runtime);
            tvGenre = (TextView) view.findViewById(R.id.tv_genre);
            tvLanguage = (TextView) view.findViewById(R.id.tv_language);
            tvCountry = (TextView) view.findViewById(R.id.tv_country);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            int itemPosition = getItemPosition();
            onItemclicked(v, itemPosition, getItem(itemPosition));
        }
    }

    private void setupImageLoadConfigurations() {

        DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder();
        builder.cacheInMemory(true);
        builder.cacheOnDisk(true).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                IMDBApplication.getInstance()).defaultDisplayImageOptions(builder.build()).build();

        ImageLoader.getInstance().init(config);
    }
}