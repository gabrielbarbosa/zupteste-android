package com.movie.imdb.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.movie.imdb.R;

public class UiMessages {

    private Activity activity;
    private ProgressDialog pDialog;

    public UiMessages(Activity activity) {

        this.activity = activity;
    }

    public void showShortToast(String message) {

        if (message != null) {

            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
        }
    }

    public void showLongToast(String message) {

        if(message != null) {

            Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Exibe um diálogo de carregamento ao usuário
     *
     * @param message    mensagem a ser exibida
     * @param cancelable diálogo pode ser cancelado pelo usuário
     */
    public void showProgressDialogLoad(String message, boolean cancelable) {

        dismissProgressDialogLoad();
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage(message);
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(cancelable);
        pDialog.show();
    }

    /**
     * Fecha o diálogo de carregamento
     */
    public void dismissProgressDialogLoad() {

        try {

            if (pDialog != null && pDialog.isShowing()) {

                pDialog.dismiss();
                pDialog = null;
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void showDialogAlert(String message, String lblButton, boolean cancelable, final DialogInterface.OnClickListener onClickListener) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(activity.getResources().getString(R.string.app_name))
                .setMessage(message).setCancelable(cancelable)
                .setPositiveButton(lblButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (onClickListener != null) {

                            onClickListener.onClick(dialogInterface, i);
                        }
                    }
                });

        builder.create().show();
    }

    public void showDialogConfirm(String message, String lblButton1, final DialogInterface.OnClickListener actionButton1) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(activity.getResources().getString(R.string.app_name))
                .setMessage(message).setCancelable(false)
                .setPositiveButton(lblButton1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (actionButton1 != null) {

                            actionButton1.onClick(dialogInterface, i);
                        }
                    }
                });

        builder.create().show();
    }

    public void showToastShort(int msgId) {

        Toast.makeText(activity, activity.getString(msgId), Toast.LENGTH_SHORT).show();
    }

    public void showToastShort(String msg) {

        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
    }

    public void showToastLong(String msg) {

        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }

    public void showLongSnackBar(View anchorView, String message) {

        Snackbar snackbar = Snackbar.make(anchorView, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public void showShortSnackBar(View anchorView, String message) {

        Snackbar snackbar = Snackbar.make(anchorView, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
}