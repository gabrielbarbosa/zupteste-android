package com.movie.imdb.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.movie.imdb.R;
import com.movie.imdb.controller.MoviesController;
import com.movie.imdb.interfaces.AbstractRequestListener;
import com.movie.imdb.interfaces.ClickListener;
import com.movie.imdb.interfaces.SugarListener;
import com.movie.imdb.model.Movie;
import com.movie.imdb.model.WsError;
import com.movie.imdb.view.adapter.MovieAdapter;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.apache.commons.lang3.StringUtils;
import org.parceler.Parcels;

import java.util.List;

import de.greenrobot.event.EventBus;

public class SearchMovieFragment extends BaseFragment {

    private static final String KEY_MOVIE = "key_movie";

    private MoviesController moviesController;
    private String lastTitle;
    private MovieAdapter movieAdapter;

    // Views
    private View view;
    private RelativeLayout rlContainerLoad;
    private ProgressBar pbLoading;
    private TextView tvError;
    private RecyclerView rvMovies;
    private LinearLayoutManager linearLayoutManager;
    private SearchView mSearchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.search_movie_fragment, container, false);

        movieAdapter = new MovieAdapter(getActivity());
        moviesController = new MoviesController(getRequestController());

        setScreenComponents();
        setListeners();
        setHasOptionsMenu(true);
        setMessageError(getString(R.string.lbl_search_movies), null);

        return view;
    }

    @Override
    public void onResume() {

        super.onResume();

        if (hasRestorableRequest()) {

            showLoading(true);
            return;
        }

        if (movieAdapter.isEmpty()) {

            showEmptyList();

        } else {

            showLoading(false);
        }
    }

    @Override
    public void setScreenComponents() {

        rlContainerLoad = (RelativeLayout) view.findViewById(R.id.rl_container_load);
        pbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);
        tvError = (TextView) view.findViewById(R.id.tv_error);
        rvMovies = (RecyclerView) view.findViewById(R.id.rv_movies);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvMovies.setLayoutManager(linearLayoutManager);
        rvMovies.setItemAnimator(new DefaultItemAnimator());

        rvMovies.setAdapter(movieAdapter);
    }

    @Override
    public void setListeners() {

        movieAdapter.setClickListener(itemMovieClickListener);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_search, menu);

        final MenuItem searchItem = menu.findItem(R.id.menu_search);

        if (searchItem != null) {

            mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            mSearchView.clearFocus();
            mSearchView.setOnQueryTextListener(listMoviesOnQueryTextListener);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {

        if (bundle != null) {

            bundle.putParcelable(KEY_MOVIE, Parcels.wrap(movieAdapter.getItensList()));
        }

        super.onSaveInstanceState(bundle);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle bundle) {

        super.onViewStateRestored(bundle);

        if (bundle == null) {

            return;
        }

        List<Movie> movieList = Parcels.unwrap(bundle.getParcelable(KEY_MOVIE));

        if (movieList != null) {

            movieAdapter.addItensAndNotifyDataSet(movieList.toArray(new Movie[movieList.size()]));
        }
    }

    //==============================================================================================
    // Métodos privados
    //==============================================================================================

    private AbstractRequestListener<Movie> searchMoviesAbstractRequestListener = new AbstractRequestListener<Movie>() {
        @Override
        public void onRequestFailure(WsError wsError) {

            if (!isAdded()) {

                return;
            }

            showEmptyList();
        }

        @Override
        public void onRequestNotFound() {
        }

        @Override
        public void onRequestSuccess(Movie movie) {

            if (!isAdded()) {

                return;
            }

            if (StringUtils.isEmpty(movie.getTitle())) {

                showEmptyList();
                return;
            }

            movieAdapter.clear();
            movieAdapter.addItensAndNotifyDataSet(movie);
            saveMovie(movie);
        }
    };

    private SugarListener saveMovieListener = new SugarListener() {

        @Override
        public void onSucess(Movie... movies) {

            if (!isAdded()) {

                return;
            }

            showLoading(false);
            postMovieEvent(movies[0]);
        }

        @Override
        public void onError(Exception exception) {

            if (!isAdded()) {

                return;
            }

            showLoading(false);
            getUiMessages().showLongToast(getString(R.string.lbl_failed_save_data));
        }
    };

    private ClickListener<Movie> itemMovieClickListener = new ClickListener<Movie>() {
        @Override
        public void onItemClick(View view, int position, Movie data) {
            // TODO
        }

        @Override
        public void onLongClick(View view, int position, Movie data) {
        }
    };

    private SearchView.OnQueryTextListener listMoviesOnQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {

            if (!StringUtils.equals(s, lastTitle)) {

                lastTitle = s;
                searchMovie(s);
            }

            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {

            return false;
        }
    };

    private void showLoading(boolean show) {

        if (show) {

            rlContainerLoad.setVisibility(View.VISIBLE);
            pbLoading.setVisibility(View.VISIBLE);
            tvError.setVisibility(View.GONE);
            rvMovies.setVisibility(View.GONE);

        } else {

            rlContainerLoad.setVisibility(View.GONE);
            rvMovies.setVisibility(View.VISIBLE);
        }
    }

    private void setMessageError(String message, View.OnClickListener onClickListener) {

        tvError.setText(message);
        tvError.setOnClickListener(onClickListener);

        tvError.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.GONE);
        rvMovies.setVisibility(View.GONE);
        rlContainerLoad.setVisibility(View.VISIBLE);
    }

    private void showEmptyList() {

        tvError.setText(getString(R.string.lbl_no_movie_found));
        tvError.setVisibility(View.VISIBLE);
        rlContainerLoad.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.GONE);
        rvMovies.setVisibility(View.GONE);
    }

    private void searchMovie(String titleMovie) {

        showLoading(true);
        moviesController.searchMovie(titleMovie, searchMoviesAbstractRequestListener);
    }

    private void saveMovie(Movie movie) {

        moviesController.saveMovie(movie, saveMovieListener);
    }

    private void postMovieEvent(Movie movie) {

        EventBus.getDefault().post(movie);
    }
}
