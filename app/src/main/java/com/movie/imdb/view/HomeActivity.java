package com.movie.imdb.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.movie.imdb.R;

public class HomeActivity extends BaseAppCompatActivity {

    //Views
    private Toolbar toolbar;
    private FloatingActionButton fabAddMovie;
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setScreenComponents();
        setListeners();
    }

    @Override
    public void setScreenComponents() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        fabAddMovie = (FloatingActionButton) findViewById(R.id.fab_add_movie);

        setupToolbar(toolbar, null, getString(R.string.lbl_home));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        fragment = getSupportFragmentManager().findFragmentById(R.id.fr_home);
    }

    @Override
    public void setListeners() {

        fabAddMovie.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(fragment instanceof BaseFragment) {

            ((BaseFragment)fragment).onClick(view);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (fragment != null) {

            fragment.onActivityResult(requestCode, resultCode, data);

        } else {

            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    //==============================================================================================
    // Métodos privados
    //==============================================================================================
}
