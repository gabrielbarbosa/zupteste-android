package com.movie.imdb.view.helper;

import android.app.Activity;
import android.content.Intent;

import com.movie.imdb.view.SearchMovieActivity;

public class NavigationHelper {

    public static void openSearchMovies(Activity activity) {

        activity.startActivity(new Intent(activity, SearchMovieActivity.class));
    }
}
