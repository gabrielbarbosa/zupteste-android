package com.movie.imdb.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.movie.imdb.R;

public class SearchMovieActivity extends BaseAppCompatActivity {

    //Views
    private Toolbar toolbar;
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_movie);
        setScreenComponents();
    }

    @Override
    public void setScreenComponents() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setupToolbar(toolbar, null, getString(R.string.lbl_search));
        fragment = getSupportFragmentManager().findFragmentById(R.id.fr_search_movie);
    }

    @Override
    public void setListeners() {
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (fragment != null) {

            fragment.onActivityResult(requestCode, resultCode, data);

        } else {

            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //==============================================================================================
    // Métodos privados
    //==============================================================================================
}
