package com.movie.imdb.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;

import com.movie.imdb.controller.RequestController;
import com.movie.imdb.interfaces.AbstractRequestListener;
import com.movie.imdb.interfaces.BaseActivityInterface;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.EventBusException;

public abstract class BaseFragment extends Fragment implements OnClickListener, BaseActivityInterface {

    private RequestController requestController;
    private UiMessages uiMessages;
    private boolean isDestroyed;
    private boolean isPaused;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        isDestroyed = false;
        requestController = new RequestController();
        uiMessages = new UiMessages(getActivity());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        try {

            EventBus.getDefault().registerSticky(this);

        } catch (EventBusException ignored) {
        }
    }

    @Override
    public void onStart() {

        super.onStart();
        requestController.start(); isDestroyed = true;
    }

    @Override
    public void onPause() {

        super.onPause();
        isPaused = true;
    }

    @Override
    public final RequestController getRequestController() {

        return requestController;
    }

    @Override
    public final UiMessages getUiMessages() {

        return uiMessages;
    }

    @Override
    public final boolean hasRestorableRequest() {

        return (requestController != null && requestController.hasRequestSend());
    }

    @Override
    public boolean hasRestorableRequestForListener(AbstractRequestListener abstractRequestListener) {

        return (requestController != null && requestController.hasRestorableRequestForListener(abstractRequestListener));
    }

    @Override
    public AbstractRequestListener[] getAllRestorableRequestListeners() {

        return null;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        isDestroyed = true;

        try {

            EventBus.getDefault().unregister(this);

        } catch (EventBusException e) {
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        isPaused = false;
    }

    @Override
    public boolean isPaused() {
        return isPaused;
    }

    @Override
    public boolean isDestroyed() {
        return isDestroyed;
    }

    protected void setTitleToolbar(String title) {

        if(getActivity() instanceof BaseAppCompatActivity) {

            ((BaseAppCompatActivity) getActivity()).setTitleToolbar(title);
        }
    }

    protected void setSubTitleToolbar(String subTitle) {

        if(getActivity() instanceof BaseAppCompatActivity) {

            ((BaseAppCompatActivity) getActivity()).setSubTitleToolbar(subTitle);
        }
    }

    protected void setupToolbar(Toolbar toolbar, Integer resId) {

        if(getActivity() instanceof BaseAppCompatActivity) {

            ((BaseAppCompatActivity) getActivity()).setupToolbar(toolbar, resId);
        }
    }

    protected void setupToolbar(Toolbar toolbar, Integer resId, String title) {

        if(getActivity() instanceof BaseAppCompatActivity) {

            ((BaseAppCompatActivity) getActivity()).setupToolbar(toolbar, resId, title);
        }
    }
}
