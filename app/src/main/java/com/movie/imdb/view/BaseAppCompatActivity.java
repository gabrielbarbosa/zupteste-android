package com.movie.imdb.view;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.movie.imdb.controller.RequestController;
import com.movie.imdb.interfaces.AbstractRequestListener;
import com.movie.imdb.interfaces.BaseActivityInterface;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.EventBusException;

public abstract class BaseAppCompatActivity extends AppCompatActivity implements BaseActivityInterface, View.OnClickListener {

    private static final String TAG_BASE = BaseAppCompatActivity.class.getSimpleName();

    private boolean isPaused;
    private boolean isDestroyed;

    private UiMessages uiMessages;
    private RequestController requestController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        isDestroyed = false;
        uiMessages = new UiMessages(this);
        requestController = new RequestController();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {

        super.onPostCreate(savedInstanceState);

        try {

            EventBus.getDefault().registerSticky(this);

        } catch (EventBusException ignored) {
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
        isPaused = false;
    }

    @Override
    public boolean isPaused() {

        return isPaused;
    }

    @Override
    public boolean isDestroyed() {

        return isDestroyed;
    }

    @Override
    public void onPause() {

        super.onPause();
        isPaused = true;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        isDestroyed = true;
        EventBus.getDefault().unregister(this);
    }

    @Override
    public AbstractRequestListener[] getAllRestorableRequestListeners() {

        return null;
    }

    @Override
    public RequestController getRequestController() {

        return requestController;
    }

    @Override
    public UiMessages getUiMessages() {

        return uiMessages;
    }

    @Override
    public boolean hasRestorableRequest() {

        return requestController != null && requestController.hasRequestSend();
    }

    @Override
    public boolean hasRestorableRequestForListener(AbstractRequestListener abstractRequestListener) {

        return (requestController != null && requestController.hasRestorableRequestForListener(abstractRequestListener));
    }

    protected void setTitleToolbar(String title) {

        if (getSupportActionBar() != null && title != null) {

            getSupportActionBar().setTitle(title);
        }
    }

    protected void setSubTitleToolbar(String subTitle) {

        if (getSupportActionBar() != null && subTitle != null) {

            getSupportActionBar().setSubtitle(subTitle);
        }
    }

    protected void setupToolbar(Toolbar toolbar, Integer resId) {

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            if(resId != null) {

                getSupportActionBar().setHomeAsUpIndicator(resId);
            }

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    protected void setupToolbar(Toolbar toolbar, Integer resId, String title) {

        if(toolbar != null) {

            setupToolbar(toolbar, resId);
            setTitleToolbar(title);
        }
    }

    protected void hideSoftKeyboard(View view) {

        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    //==============================================================================================
    // Métodos privados
    //==============================================================================================
}